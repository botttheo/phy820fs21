{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# P02: Solar System Simulator\n",
    "\n",
    "* Last revision: 27-Oct-2021 by Heiko Hergert (hergert@frib.msu.edu)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-warning\">\n",
    "\n",
    "**First draft due:** Nov 17, 2021\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Introduction\n",
    "In this project we create a 3D simulation of the solar system - or, really, a simulation of a general $N$-body system with gravitational interactions. (For even greater generality, we might have to worry about implementing behavior at or near collisions, but that becomes more of an issue if we have to simulate many thousands of objects, and give them random starting positions and velocities.)\n",
    "\n",
    "The visualization is achieved with VPython, which provides a very accessible way of creating 3D scenes and animations. To avoid clutter, the class definitions for the simulator and the simulated objects have been moved into the module `p02_simulator.py`, and are imported from there.\n",
    "\n",
    "Running from within a Jupyter notebook, VPython may turn into a bit of a \"resource hog\", so I also provided a standalone Python module that you can use for your work by running\n",
    "```\n",
    "    python p02_solar_system.py\n",
    "```\n",
    "\n",
    "from the **command line**. (You can also follow standard Python practices to convert the file into an executable.)\n",
    "\n",
    "In the following, I will briefly discuss the design of the `Simulator` and `AstroObject` classes, as well as the mathematics of the routines that are used to integrate the equations of motion.\n",
    "\n",
    "\n",
    "## 1. Class Design\n",
    "\n",
    "### 1.1 AstroObject\n",
    "`AstroObject` is a class that is derived from VPpython's `sphere` class. The parent class provides the means to track the position of the object in 3D space, and the functionality to visualize it. The derived class adds additional parameters that hold the properties of the physical object (mass, velocity, etc.), and provides routines for computing forces and energies.\n",
    "\n",
    "### 1.2 Simulator\n",
    "The state of the interacting system cannot be completely encapsulated in the individual `AstroObject` structures, because we need to update the positions, velocities and forces of **all** objects consistently when we perform time steps, since they are coupled in the equations of motion.\n",
    "\n",
    "Thus, the time evolution is performed in the `Simulator` class, using different approximations for executing the time steps (see next section). The `Simulator` class also provides routines for interfacing with its child `AstroObject` classes, and for computing properties of the system as a whole, e.g., the center of mass $\\vec{R}$ or total momentum $\\vec{P}$. \n",
    "\n",
    "\n",
    "## 2. Integrators\n",
    "Previously, we have used ODE integrators from SciPy (or the libraries that it is built upon). These solvers are accurate and reasonably efficient for general purposes, but here, we will see that it can be very beneficial to use integrators that explicitly preserve the **geometric structure** of a problem. Here, I am not talking about the geometry of the solar system in coordinate space, but rather that of its **phase space**. When we discuss Hamiltonian mechanics, we will find that phase spaces are so-called **symplectic manifolds** (more details later) - hence, the ODE solvers that are aware of the geometry of phase space are referred to as **symplectic integrators**. \n",
    "\n",
    "### 2.1 Forward Euler Method\n",
    "The forward Euler method is the simplest approach to integrating a system of ODEs. Like all ODE solvers, it relies on the Taylor expansion of the functions that are to be integrated, here the position and velocity vectors:\n",
    "$$\n",
    "\\begin{align}\n",
    "    \\vec{r}(t+\\Delta t) &= \\vec{r}(t) + \\left.\\frac{d\\vec{r}}{dt}\\right|_t\\Delta t + O(\\Delta t^2)\\\\\n",
    "    \\vec{v}(t+\\Delta t) &= \\vec{v}(t) + \\left.\\frac{d\\vec{a}}{dt}\\right|_t\\Delta t + O(\\Delta t^2)\\,,\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "which we can rewrite as the coupled system\n",
    "\n",
    "$$\n",
    "    \\begin{align}\n",
    "       \\vec{r}(t+\\Delta t) &= \\vec{r}(t) + \\vec{v}(t)\\Delta t + O(\\Delta t^2)\\\\\n",
    "       \\vec{v}(t+\\Delta t) &= \\vec{v}(t) + \\frac{\\vec{F}(t)}{m}\\Delta t + O(\\Delta t^2)\\,.\n",
    "    \\end{align}\n",
    "$$\n",
    "\n",
    "Thus, we only need to know $\\vec{r}(t)$, $\\vec{v}(t)$ and $\\vec{F}(t)$ to perform a time step at any given point. \n",
    "\n",
    "Given the nature of these approximations, it is clear that we will accumulate substantial errors along the integration unless the size $\\Delta t$ of a single step is very small - this is related to the **relative error** of the method. You may think that we should just keep the step size small, but this is problematic as well: We are usually interested in integrating a problem from $t_\\text{initial}$ to $t_\\text{final}$, and a small $\\Delta t$ means that we will need to perform *many* steps to integrate - the smaller $\\Delta t$, the more steps we'll need, and the higher the accumulated **absolute error** consisting of the summed relative errors, roundoff due to finite-precision math etc. is going to be. Any numerical ODE solver must therefore try to strike a balance between relative and absolute errors. \n",
    "\n",
    "Improvements on the Forward Euler algorithm exploit techniques like Richardson extrapolation - that is, the combination of time steps for certain rations of step sizes - as well as approximations to the higher derivatives that would appear in the Taylor series to reduce the relative and absolute errors. Prominent examples are the Runge-Kutta methods and the integrators discussed in the following.\n",
    "\n",
    "\n",
    "### 2.2 Velocity Verlet\n",
    "One of the simplest symplectic integrators is the **velocity Verlet method**. It is based on the following approximation for the time step:\n",
    "\n",
    "$$\n",
    "    \\begin{align}\n",
    "        \\vec{r}(t+\\Delta t) &= \\vec{r}(t) + \\vec{v}(t)\\Delta t + \\frac{1}{2}\\vec{a}(t)\\Delta t^2\\,,\\\\\n",
    "        \\vec{v}(t+\\Delta t) &= \\vec{v}(t) + \\frac{1}{2}\\left(\\vec{a}(t) + \\vec{a}(t+\\Delta t\\right)\\Delta t\\,, \n",
    "    \\end{align}\n",
    "$$\n",
    "\n",
    "which immediately tells us that it is of higher order than the Forward Euler method: The position updates are accurate through second order in the time step.\n",
    "\n",
    "The algorithm is typically implemented as follows: \n",
    "\n",
    "$$\n",
    "    \\begin{align}\n",
    "        \\vec{v}\\left(t+\\frac{1}{2}\\Delta t\\right) &= \\vec{v}(t) + \\frac{1}{2}\\vec{a}(t) \\Delta t\\,,\\\\\n",
    "        \\vec{r}\\left(t+\\Delta t\\right) &= \\vec{r}(t) + \\vec{v}\\left(t+\\frac{1}{2}\\Delta t\\right)\\Delta t\\,,\\\\\n",
    "        \\vec{a}\\left(t+\\Delta t\\right) &= \\frac{1}{m}\\vec{F}\\left(t+\\Delta t\\right)\\,,\\\\\n",
    "        \\vec{v}\\left(t+\\Delta t\\right) &= \\vec{v}\\left(t+\\frac{1}{2}\\Delta t\\right) + \\frac{1}{2}\\vec{a}\\left(t+\\Delta t\\right)\\Delta t\\,.\n",
    "    \\end{align}\n",
    "$$\n",
    "\n",
    "The velocity at the half time-step can be eliminated to yield\n",
    "\n",
    "$$\n",
    "    \\begin{align}\n",
    "        \\vec{r}\\left(t+\\Delta t\\right) &= \\vec{r}(t) + \\vec{v}(t)\\Delta t + \\frac{1}{2}\\vec{a}(t) \\Delta t^2\\,,\\\\\n",
    "        \\vec{a}\\left(t+\\Delta t\\right) &= \\frac{1}{m}\\vec{F}\\left(t+\\Delta t\\right)\\,,\\\\\n",
    "        \\vec{v}\\left(t+\\Delta t\\right) &= \\vec{v}(t) + \\frac{1}{2}\\left(\\vec{a}(t) + \\vec{a}\\left(t+\\Delta t\\right)\\right)\\Delta t\n",
    "    \\end{align}\\,,\n",
    "$$\n",
    "\n",
    "which is the form implemented in the `Simulator` class. Because of the alternating position and velocity updates, velocity Verlet (and more general symplectic schemes) are sometimes referred to (and grouped with) **leapfrog methods** for solving ODEs.\n",
    "\n",
    "### 2.3 Forrest-Ruth\n",
    "\n",
    "The Forrest-Ruth method is a fourth-order symplectic integrator defined by  \n",
    "\n",
    "$$\n",
    "    \\begin{align}\n",
    "        \\vec{r}_1 &= \\vec{r}_0 + c_1 \\vec{v}_0\\Delta t\\,,\\\\\n",
    "        \\vec{v}_1 &= \\vec{v}_0 + d_1 \\vec{a}_1\\Delta t = \\vec{v}_0 + d_1 \\frac{\\vec{F}(\\vec{r}_1)}{m}\\Delta t\\,,\\\\\n",
    "        \\vec{r}_2 &= \\vec{r}_1 + c_2 \\vec{v}_1\\Delta t\\,,\\\\\n",
    "        \\vec{v}_2 &= \\vec{v}_1 + d_2 \\frac{\\vec{F}(\\vec{r}_2)}{m}\\Delta t\\,,\\\\\n",
    "        \\vec{r}_3 &= \\vec{r}_2 + c_3 \\vec{v}_2\\Delta t\\,,\\\\\n",
    "        \\vec{v}_3 &= \\vec{v}_2 + d_3 \\frac{\\vec{F}(\\vec{r}_3)}{m}\\Delta t\\,,\\\\        \n",
    "        \\vec{r}_4 &= \\vec{r}_3 + c_4 \\vec{v}_3\\Delta t\\,.\n",
    "    \\end{align}\\,,\n",
    "$$\n",
    "\n",
    "Defining $K\\equiv\\frac{1}{2-2^{(1/3)}}$, the coefficients $c_i$ and $d_i$ are given by\n",
    "\n",
    "$$\n",
    "    \\begin{align}\n",
    "        c_1 = c_4 &= \\frac{K}{2}\\,,\\\\\n",
    "        c_2 = c_3 &= \\frac{1-K}{2}\\,,\\\\\n",
    "        d_1 = d_3 &=  K\\,,\\\\\n",
    "        d_2 &= (1-2K)\\,.\n",
    "    \\end{align}\n",
    "$$\n",
    "You can verify that at the end of a step, we have \n",
    "\n",
    "$$\n",
    "    \\begin{equation}\n",
    "        \\vec{r}_4 = \\vec{r}(t+\\Delta t)\\,,\\quad \\vec{v}_3 = \\vec{v}(t+\\Delta t)\\,. \n",
    "    \\end{equation}\n",
    "$$\n",
    "\n",
    "### 2.4 Symmetry under Time Reversal\n",
    "An integration method is symmetric under time reversal if the algorithm is invariant under the replacements\n",
    "\n",
    "$$\n",
    "    \\begin{equation}\n",
    "        \\Delta t \\rightarrow - \\Delta t\\,, \\quad a_{n+j} \\rightarrow a_{n-j}\\,, \n",
    "    \\end{equation}\n",
    "$$\n",
    "\n",
    "where $a_{n} = a(t_n)$ is the numerical approximation to the solution at the time $t_n=t_0 + n\\Delta t$. It is easy to see that the forward Euler method is not invariant under these operations, while the velocity Verlet and Forrest-Ruth methods (and symplectic methods in general) are. \n",
    "\n",
    "For time-reversal invariant methods, the total energy of the system will perform bounded oscillations around a central value, without substantial energy drift (i.e., accumulated error). For that reason, they are used for long-term simulations of the stability of our solar system.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Solar System Simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div id=\"glowscript\" class=\"glowscript\"></div>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/javascript": [
       "if (typeof Jupyter !== \"undefined\") { window.__context = { glowscript_container: $(\"#glowscript\").removeAttr(\"id\")};}else{ element.textContent = ' ';}"
      ],
      "text/plain": [
       "<IPython.core.display.Javascript object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "from time import sleep\n",
    "\n",
    "import numpy as np\n",
    "from p02_simulator import AstroObject, Simulator\n",
    "\n",
    "from vpython import *\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We should choose natural units for our simulation, so that we do not have to worry about roundoff errors when dealing with numbers that cover vastly different orders of magnitudes (e.g., $G\\sim10^{-10}$, $M \\sim 10^{30}$, etc.). **Experiment with the length scale to achieve good visualization results!**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# set up parameters\n",
    "G    = 6.67*10**(-11)   # Newton's gravitational constant in m**3 kg**(-1) s**(-2)\n",
    "\n",
    "# express everything in natural units - use years for time\n",
    "m0=5.97*10**24          # express all masses in terms of Earth's mass\n",
    "R0=149.6*10**8          # 1/10 AU (experiment with this)\n",
    "t0=24*3600*365.24\n",
    "\n",
    "G=G/(R0**3) *m0 * t0**2  # G in R0^3 m_E**(-1) years**(-2) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's create a VPython scene - all subsequent VPython commands will render output through this element. \n",
    "\n",
    "The output is created below the cell that initializes the canvas, so we need to put the remaining code in here to avoid modifying the figure *below* the point is is first rendered - a bit unfortunate. (Let me know if you figure out a way around this.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "**Tasks**: \n",
    "\n",
    "We start by only considering the Sun and the Earth in the simulation. (Document your work with (brief) discussion and snapshots, for instance.) \n",
    "\n",
    "**Make sure to look at the available routines (and their source!) in the Simulator module before you tackle any of the tasks!** \n",
    "\n",
    "1. Zoom into the simulation until you see the Sun's trajectory (in its interior). It has a spiraling shape - why is that, and what can you do to \"fix\" this?\n",
    "\n",
    "2. Check the quality of the provided ODE solvers by varying the size of the time step over several orders of magnitude, and checking conservation laws, e.g., for energy. What other conservation laws are worth checking? \n",
    "\n",
    "3. Mercury has the most eccentric orbit of the planets in the Solar system. Together with its proximity to the Sun, this makes it best suited to illustrate perihelion precession due to corrections to the gravitational force.\n",
    "    \n",
    "    a. Extend the definition of the gravitational force (as used in the  `Simulator.dvdt()`) to include corrections due to the other planets and finite-size effects ($\\sim r^{-3}$) as well as General Relativity ($\\sim r^{-4}$):\n",
    "    $$\n",
    "            \\begin{equation}\n",
    "                \\vec{F}(r) = \\frac{\\alpha}{r^3} + \\frac{\\beta}{r^4}\n",
    "            \\end{equation}\n",
    "    $$\n",
    "    \n",
    "    b. Add the planet Mercury to your simulation and demonstrate the perihelion precession for suitable values of $\\alpha, \\beta$. The parameters of planetary orbits can be found here: https://nssdc.gsfc.nasa.gov/planetary/factsheet/. **Note that you will have to let the simulation run for a sufficiently long time (i.e., many full orbits) to see the effect clearly. Make sure to use an appropriate solver when you do.** \n",
    "\n",
    "Now switch off the corrections to the gravitational potential after simulating Mercury’s orbital precession. (In principle, they differ from planet to planet because they sum up finite-size effects and GR contributions, and you may be exaggerating the real values in order to see an appreciable effect for Mercury in your simulation - that can lead to unrealistic behavior for other planets once you add them in.) \n",
    "    \n",
    "4. Add all the major planets and Pluto to your simulation.\n",
    "\n",
    "5. Use your simulator to explore two or more interesting problems: Examples can be \n",
    "    - a transfer orbit or gravitational assist of a spacecraft (see worksheet 9),\n",
    "    - the passage of a massive object - e.g., a wandering star - through the solar system from above or below the ecliptic,\n",
    "    - or an exoplanetary system (e.g., Trappist.)\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Answers\n",
    "\n",
    "\n",
    "\n",
    "1. I believe this is due to the fact that we are not in the center of mass frame. By running the routine \"transform_to_com\", we are able to change this, and then the sun moves in a very small circle. This is expected, because the overall angular momentum must be conserved.\n",
    "\n",
    "\n",
    "\n",
    "2. We have three methods: Euler, Verlet, and Forest-Ruth. With the Euler method, we don't even have energy conservation at dt = 0.001. Instead, the Earth flies around and gains some radial displacement with each pass, so it makes sense that the energy is not conserved. With the Verlet method, the accuracy breaks down if we lower dt to 0.01 instead. It makes jagged trajectory changes, instead of smooth ones. The Forest-Ruth method does the same thing. As for other conservations, we could check the angular momentum conservation. This should be conserved for any orbit as long as we place the origin on top of the object exerting the force (in this case, the sun). This is because the angular momentum is conserved when there is no torque, and if the sun is at the origin, the force and $\\vec{r}$ are always 180 degrees apart, and thus $\\vec{r}$ x $\\vec{F} $ is always zero. In a circular orbit, the magnitude of the linear momentum will be conserved as well, as the acceleration of the orbiting mass is strictly directional, and the speed is unchanged.\n",
    "\n",
    "\n",
    "\n",
    "3. We can do this by taking the dvdt function in the p02_simulator.py file and passing two additional variables, alpha and beta (defined as zero if unspecified), and incorporate them into the acceleration equation. We need to divide by the mass as well, since the force is gievn as the coefficient divided by the radial dependence, and so the acceleration must lose the implied mass multiple. To add Mercury, I created an AstrObject, plugged in the proper parameters, and added it to the list for the loop to loop through. I am unsure about the alpha and beta values; seems to me that alpha (size effects) should take into account the diameter/volume of the planet, and that beta (relativistic effects) shoudld take into account the speed of the planet relative to the speed of light. However, I don't know how these things specifically manifest.\n",
    "\n",
    "\n",
    "\n",
    "4. Same thing as with Mercury, but with the other planets and their respective data value. It's satisfying :)\n",
    "\n",
    "\n",
    "\n",
    "5. For our extension, we added a \"massivestar\", "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div id=\"glowscript\" class=\"glowscript\"></div>"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/javascript": [
       "if (typeof Jupyter !== \"undefined\") { window.__context = { glowscript_container: $(\"#glowscript\").removeAttr(\"id\")};}else{ element.textContent = ' ';}"
      ],
      "text/plain": [
       "<IPython.core.display.Javascript object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/javascript": [
       "if (typeof Jupyter !== \"undefined\") {require.undef(\"nbextensions/vpython_libraries/glow.min\");}else{element.textContent = ' ';}"
      ],
      "text/plain": [
       "<IPython.core.display.Javascript object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/javascript": [
       "if (typeof Jupyter !== \"undefined\") {require.undef(\"nbextensions/vpython_libraries/glowcomm\");}else{element.textContent = ' ';}"
      ],
      "text/plain": [
       "<IPython.core.display.Javascript object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/javascript": [
       "if (typeof Jupyter !== \"undefined\") {require.undef(\"nbextensions/vpython_libraries/jquery-ui.custom.min\");}else{element.textContent = ' ';}"
      ],
      "text/plain": [
       "<IPython.core.display.Javascript object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/javascript": [
       "if (typeof Jupyter !== \"undefined\") {require([\"nbextensions/vpython_libraries/glow.min\"], function(){console.log(\"GLOW LOADED\");});}else{element.textContent = ' ';}"
      ],
      "text/plain": [
       "<IPython.core.display.Javascript object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/javascript": [
       "if (typeof Jupyter !== \"undefined\") {require([\"nbextensions/vpython_libraries/glowcomm\"], function(){console.log(\"GLOWCOMM LOADED\");});}else{element.textContent = ' ';}"
      ],
      "text/plain": [
       "<IPython.core.display.Javascript object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/javascript": [
       "if (typeof Jupyter !== \"undefined\") {require([\"nbextensions/vpython_libraries/jquery-ui.custom.min\"], function(){console.log(\"JQUERY LOADED\");});}else{element.textContent = ' ';}"
      ],
      "text/plain": [
       "<IPython.core.display.Javascript object>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# set up the VPython scene\n",
    "scene = canvas(title='Solar System',\n",
    "            width=600, height=400,\n",
    "            center=vector(0,0,0), background=color.white)\n",
    "\n",
    "# For some reason, the creators thought it would be a good idea to have y be the\n",
    "# upward direction. We'll change that to the z direction.\n",
    "scene.forward = vector(1,0,0)\n",
    "scene.up = vector(0,0,1)\n",
    "\n",
    "# Define and initiate the simulated objects - remember to translate to natural units!\n",
    "sun     = AstroObject(G, \n",
    "                      mass = 1.99*10**30/m0, \n",
    "                      pos=vector(0,0,0),\n",
    "                      velocity=vector(0,0,0), \n",
    "                      color=vector(255, 220, 0)/255., radius=1)\n",
    "\n",
    "earth   = AstroObject(G, \n",
    "                      mass = 5.97*10**24/m0, \n",
    "                      pos=vector(147.1*10**9/R0,0,0), \n",
    "                      velocity=vector(0,29800*t0/R0,0), \n",
    "                      color=color.blue, radius=0.2)\n",
    "\n",
    "mercury = AstroObject(G, \n",
    "                      mass = 0.33*10**24/m0, \n",
    "                      pos=vector(46.0*10**9/R0,0,0), \n",
    "                      velocity=vector(0,47400*t0/R0,0), \n",
    "                      color=vector(1,0.7,0.2), radius=0.033)\n",
    "\n",
    "venus = AstroObject(G, \n",
    "                      mass = 4.87*10**24/m0, \n",
    "                      pos=vector(107.5*10**9/R0,0,0), \n",
    "                      velocity=vector(0,35000*t0/R0,0), \n",
    "                      color=color.green, radius=0.2)\n",
    "\n",
    "mars = AstroObject(G, \n",
    "                      mass = 0.642*10**24/m0, \n",
    "                      pos=vector(206.6*10**9/R0,0,0), \n",
    "                      velocity=vector(0,24100*t0/R0,0), \n",
    "                      color=color.red, radius=0.1)\n",
    "\n",
    "jupiter = AstroObject(G, \n",
    "                      mass = 1898*10**24/m0, \n",
    "                      pos=vector(740.5*10**9/R0,0,0), \n",
    "                      velocity=vector(0,13100*t0/R0,0), \n",
    "                      color=color.orange, radius=0.5)\n",
    "\n",
    "saturn = AstroObject(G, \n",
    "                      mass = 568*10**24/m0, \n",
    "                      pos=vector(1352.6*10**9/R0,0,0), \n",
    "                      velocity=vector(0,9700*t0/R0,0), \n",
    "                      color=color.purple, radius=0.4)\n",
    "\n",
    "uranus = AstroObject(G, \n",
    "                      mass = 86.8*10**24/m0, \n",
    "                      pos=vector(2741.3*10**9/R0,0,0), \n",
    "                      velocity=vector(0,6800*t0/R0,0), \n",
    "                      color=color.black, radius=0.3)\n",
    "\n",
    "neptune = AstroObject(G, \n",
    "                      mass = 102*10**24/m0, \n",
    "                      pos=vector(4444.5*10**9/R0,0,0), \n",
    "                      velocity=vector(0,5400*t0/R0,0), \n",
    "                      color=color.magenta, radius=0.3)\n",
    "\n",
    "pluto = AstroObject(G, \n",
    "                      mass = 0.0146*10**24/m0, \n",
    "                      pos=vector(4436.8*10**9/R0,0,0), \n",
    "                      velocity=vector(0,4700*t0/R0,0), \n",
    "                      color=color.cyan, radius=0.033)\n",
    "\n",
    "'''\n",
    "massivestar = AstroObject(G, \n",
    "                      mass = 10**24/m0, \n",
    "                      pos=vector(-10**11/R0,-10**11,0),\n",
    "                      velocity=vector(0,10**3*t0/R0,0), \n",
    "                      color=color.yellow, radius=0.2)\n",
    "'''''\n",
    "\n",
    "# Create the list of objects... \n",
    "objects=[sun, earth, mercury, venus, mars, jupiter, saturn, uranus, neptune, pluto]#, massivestar]\n",
    "\n",
    "# ... and initiate the simulator with time step dt.\n",
    "sim = Simulator(objects, G, dt=0.001)\n",
    "\n",
    "# Choose the time span of the simulation (in years).\n",
    "tmax = 20\n",
    "\n",
    "# Create a VPython graph object for the potential energy.\n",
    "vgraph = graph(x=800, y=0,width=600,height=600,\\\n",
    "              title = 'Potential Energy', \\\n",
    "              xtitle = 't [yr]', ytitle = 'V [m_E  R0^2 yr^-2]', \\\n",
    "              foreground = color.black, background =color.white, \\\n",
    "              xmax = tmax, xmin = 0)\n",
    "\n",
    "# All subsequently defined VPython curve objects are children of the\n",
    "# same graph until the next graph object is created.\n",
    "vcurves=[ ]\n",
    "for obj in objects:\n",
    "    vcurves.append(gcurve(color=obj.color))\n",
    "\n",
    "# Same graph for the kinetic energy...\n",
    "tgraph = graph(x=800, y=0,width=600,height=600,\\\n",
    "                  title = 'Kinetic Energy (radial + angular)', \\\n",
    "                  xtitle = 't [yr]', ytitle = 'T [m_E  R0^2 yr^-2]', \\\n",
    "              foreground = color.black, background =color.white, \\\n",
    "              xmax = tmax, xmin = 0)\n",
    "\n",
    "tcurves=[ ]\n",
    "for obj in objects:\n",
    "    tcurves.append(gcurve(color=obj.color))\n",
    "\n",
    "\n",
    "# ... and the total energy.\n",
    "egraph = graph(x=800, y=0,width=600,height=600,\\\n",
    "                  title = 'Total Energy', \\\n",
    "                  xtitle = 't [yr]', ytitle = 'E [m_E  R0^2 yr^-2]', \\\n",
    "              foreground = color.black, background =color.white, \\\n",
    "              xmax = tmax, xmin = 0)\n",
    "\n",
    "ecurves=[ ]\n",
    "for obj in objects:\n",
    "    ecurves.append(gcurve(color=obj.color))\n",
    "\n",
    "# We add one curve that contains the total energy of the entire system.\n",
    "ecurves.append(gcurve(color=vector(31,158,137)/255.))\n",
    "\n",
    "\n",
    "# Initialize step counter...\n",
    "steps = 0\n",
    "\n",
    "# ... and start the simulation.\n",
    "while steps * sim.dt < tmax:\n",
    "\n",
    "    # VPython animation rate.\n",
    "    rate(100)\n",
    "    \n",
    "    # Take a time step.\n",
    "    sim.update_forest_ruth()\n",
    "    sim.transform_to_com()\n",
    "    \n",
    "    # Update energy graphs.\n",
    "    totE = 0\n",
    "    for i, obj in enumerate(objects):\n",
    "        # if obj == earth:\n",
    "        tcurves[i].plot(steps*sim.dt, obj.T)\n",
    "        vcurves[i].plot(steps*sim.dt, obj.V)\n",
    "        ecurves[i].plot(steps*sim.dt, obj.V + obj.T)\n",
    "        totE += obj.T + 0.5*obj.V\n",
    "\n",
    "    ecurves[-1].plot(steps*sim.dt, totE)\n",
    "    \n",
    "    steps+=1\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
